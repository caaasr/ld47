extends KinematicBody2D

var c_pos
var gravity = 30
var jump_vel = 800
export var vel = Vector2(0, 0)
var gravity_down = 1
var h_speed = 100
var last_button_pressed = 'r'
var dying = false

var center_vec

#used for checking if the player was on the floor recently
#this is a bad solution that works and I don't care
var fcount = 4
var fcthres = 4

func fcclamp(f):
	return max(min(fcthres, fcount), 0)

func rotate(vec):
	# returns the angle between positive x and vec
	var xaxis = Vector2(1, 0)
	
	vec.y = -vec.y
	var r = (vec.dot(xaxis))
	
	r = acos(r)
	if (vec.y < 0):
		r = (PI - r) + PI
		
	#print(r * (180/PI))
	return -r + PI/2
	
func update_pos(center, dir):
	var center_vec = (self.position - center) # changed this from - to +
	var fall_vec = dir * center_vec.normalize()
	return
	
func jumppad(s):
	var launch = -center_vec * s
	vel += launch

# Called when the node enters the scene tree for the first time.
func _ready():
	var center = get_node("../Center")
	c_pos = center.position
	
	vel = Global.velocity
	
	dying = false
	$AstroSprite.play("idle_r")

func _physics_process(delta):
	# calculate gravity direction and force
	center_vec = (self.position - c_pos).normalized()
	var down_dir = gravity_down * center_vec
	var hmove = 0
	
	#left/right handling
	if Input.is_action_pressed("ui_left") and dying == false:
		$AstroSprite.play("walk_l")
		last_button_pressed = 'l'
		hmove = -h_speed
		
	elif Input.is_action_pressed("ui_right") and dying == false:
		$AstroSprite.play("walk_r")
		last_button_pressed = 'r'
		hmove = h_speed
		
	else:
		if last_button_pressed == 'r' and dying == false:
			$AstroSprite.play("idle_r")
		elif dying == false:
			$AstroSprite.play("idle_l")
		hmove = 0
		
	var hvel = Vector2(center_vec.y, -center_vec.x) * hmove
	
	#jump handling
	if Input.is_action_just_pressed("ui_accept") and fcount > 0:
		Sounds.playJump()
		vel += -center_vec * jump_vel
		fcount = 0
	
	#rotate player to match ring
	self.rotation = rotate(center_vec) - PI

	if Global.paused == false:
		if gravity_down == -1:
			vel = move_and_slide(vel + (down_dir * gravity) + hvel, -(self.position - c_pos)) * .99

		elif not is_on_floor():
			vel = move_and_slide(vel + (down_dir * gravity) + hvel, -(self.position - c_pos)) * .99

		else:
			vel = move_and_slide(vel + hvel, -(self.position - c_pos)) * .99
			
	vel -= (hvel * 0.9)
	
	if is_on_floor():
		fcount += 2
		fcount = fcclamp(fcount)
	else:
		fcount -= 1
		fcount = fcclamp(fcount)
		
	Global.velocity = vel
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	return
#	if Input.is_action_pressed("ui_accept"):
#		gravity_down = -1
#	else:
#		gravity_down = 1
