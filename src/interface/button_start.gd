extends Button

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

func _on_button_start_pressed():
	Global.current_level = 1
	Sounds.playButton()
	get_tree().change_scene("res://scenes/levels/level_1.tscn")

func _on_button_start_mouse_entered():
	Sounds.playHover()

func _on_button_start_mouse_exited():
	# self.rect_scale = Vector2(1, 1)
	pass
