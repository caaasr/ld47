extends Button

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#    pass

func _on_button_quit_pressed():
	Sounds.playButton()
	get_tree().quit()

func _on_button_quit_mouse_entered():
	Sounds.playHover()

func _on_button_quit_mouse_exited():
	# self.rect_scale = Vector2(1, 1)
	pass
