extends Button


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_pause_title_pressed():
	Sounds.playButton()
	get_tree().change_scene("res://scenes/main_menu.tscn")
	Global.deathcount_level = 0
	Global.deathcount_total = 0
	Global.velocity = Vector2(0, 0)
	Global.paused = false


func _on_pause_title_mouse_entered():
	Sounds.playHover()
