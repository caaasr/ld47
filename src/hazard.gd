extends Area2D

var dead = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var entities = get_overlapping_bodies()
	for entity in entities:
		if entity.name == 'Astronaut' && dead == false:
			# entity.vel.x = 0
			# entity.vel.y = 0
			dead = true
			
			if (Global.paused != true):
				Global.paused = true
				Global.deathcount_level += 1
				Global.deathcount_total += 1
				
			Sounds.playDeath()
			entity.dying = true
			entity.get_child(0).stop()
			entity.get_child(0).play("death")
			self.get_parent().get_child(0).start()

func _on_DeathTimer_timeout():
	# print('Timeout.')
	Global.level_key[Global.current_level] = false
	get_tree().reload_current_scene()
	Global.paused = false
	Global.velocity = Vector2(0, 0)
