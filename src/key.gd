extends Area2D

# export(int) var level
var collected = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var entities = get_overlapping_bodies()
	for entity in entities:
		if entity.name == 'Astronaut' and collected == false:
			collected = true
			Global.level_key[Global.current_level] = true
			Sounds.playKey()
			self.hide()
