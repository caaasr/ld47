extends Area2D

var used = false
export var strength = 1000

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var entities = get_overlapping_bodies()
	for entity in entities:
		if entity.name == 'Astronaut' && used == false:
			used = true
			Sounds.playJumppad()
			entity.jumppad(strength)
			# entity.fcount = 0
			# print("Jumping")
			
		else:
			used = false
