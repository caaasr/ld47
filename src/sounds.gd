extends Node

func playButton():
	$button.play()
	
func playDeath():
	$death.play()
	
func playExit():
	$exit.play()
	
func playJump():
	$jump.play()
	
func playJumppad():
	$jumppad.play()
	
func playKey():
	$key.play()

func playHover():
	$hover.play()
	
func playMusic():
	$music.play()
