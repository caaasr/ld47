extends Area2D

# export(int) var level

export var key_required = false
var key_acquired = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if self.key_required == false:
		$Flag.play("waving")
	else:
		$Flag.play("flaccid")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if self.key_required and Global.level_key[Global.current_level] and key_acquired == false:
		key_acquired = true
		$Flag.play("waving")
	var entities = get_overlapping_bodies()
	for entity in entities:
		if entity.name == 'Astronaut':
			if self.key_required == false:
				# if (Global.current_level < Global.level_scene.size() - 1): # prevents game crash when out of levels
				Sounds.playExit()
				Global.deathcount_level = 0
				Global.current_level = Global.current_level + 1
				get_tree().change_scene(Global.level_scene[Global.current_level])
				
			elif Global.level_key[Global.current_level]:
				Sounds.playExit()
				Global.deathcount_level = 0
				Global.current_level = Global.current_level + 1
				get_tree().change_scene(Global.level_scene[Global.current_level])
