extends Node

# i have this set up as a singleton, so you can throw whatever you'd like in here
# and access it from anywhere with the Global. prefix

var musicStarted = false

var deathcount_level = 0
var deathcount_total = 0
var current_level = 1

var last_position = Vector2(0, 0)

var paused = false

var velocity = Vector2(0, 0)

# var start_level = { # starting positions for each level:
	# 1 : Vector2(320, 524),
	# 2 : Vector2(320, 524)
# }

var level_scene = { # paths to each level scene:
	1 : "res://scenes/levels/level_1.tscn",
	2 : "res://scenes/levels/level_2.tscn",
	3 : "res://scenes/levels/level_3.tscn",
	4 : "res://scenes/levels/level_4.tscn",
	5 : "res://scenes/levels/level_5.tscn",
	6 : "res://scenes/levels/level_6.tscn",
	7 : "res://scenes/levels/level_7.tscn",
	8 : "res://scenes/levels/level_8.tscn",
	9 : "res://scenes/levels/level_9.tscn",
	10 : "res://scenes/levels/level_10.tscn",
	11 : "res://scenes/levels/level_11.tscn",
	12 : "res://scenes/levels/level_12.tscn",
	13 : "res://scenes/levels/level_13.tscn",
	14 : "res://scenes/levels/level_14.tscn",
	15 : "res://scenes/levels/level_15.tscn",
	16 : "res://scenes/end_screen.tscn"
}

var level_key = { # has the level's key been picked up (if it has one)
	1 : false,
	2 : false,
	3 : false,
	4 : false,
	5 : false,
	6 : false,
	7 : false,
	8 : false,
	9 : false,
	10 : false,
	11 : false,
	12 : false,
	13 : false,
	14 : false,
	15 : false
}

func apply_last_vel(player):
	pass
